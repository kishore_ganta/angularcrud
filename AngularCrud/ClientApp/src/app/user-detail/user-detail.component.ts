import { Component, OnInit } from '@angular/core';
import { UserDetailService } from '../user-detail.service';
import { UserDetailDTO } from '../UserDetailDTO';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-UserDetail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.less']
})
export class UserDetailComponent implements OnInit {

  constructor(private UserDetailservice: UserDetailService) { }

  ngOnInit() {
    this.GetAllUserDetails();
  }
  UserDetailList: UserDetailDTO[];
  UserDetail: UserDetailDTO;
  UserDetailIdUpdate = null;
  UserDetailForm = new FormGroup({
    Name: new FormControl('', Validators.required),
    Price: new FormControl(''),
  });
  OnSubmit() {
    if (this.UserDetailIdUpdate == null) {
      const UserDetail = this.UserDetailForm.value;
      this.InsertUserDetail(UserDetail);
    }
    else {
      const employee = this.UserDetailForm.value;
      this.UpdateUserDetail(employee);
    }
  }

  GetAllUserDetails() {

    this.UserDetailservice.GetUserDetails().subscribe(data => { this.UserDetailList = data; });
  }
  GetUserDetailById(Id: string) {
    this.UserDetailservice.GetUserDetailById(Id).subscribe(data => {
      this.SetUserDetailFormValues(data);
    });

  }
  SetUserDetailFormValues(UserDetail: UserDetailDTO) {
    this.UserDetailForm.controls['UserName'].setValue(UserDetail.UserName);
    this.UserDetailForm.controls['Password'].setValue(UserDetail.Password);
    this.UserDetailIdUpdate = UserDetail.ID;
  }
  InsertUserDetail(UserDetail: UserDetailDTO) {
    this.UserDetailservice.InsertUserDetail(UserDetail).subscribe(() => {
      this.GetAllUserDetails();
    });
  }
  UpdateUserDetail(UserDetail: UserDetailDTO) {
    UserDetail.ID = this.UserDetailIdUpdate;
    this.UserDetailservice.UpdateUserDetail(UserDetail).subscribe(() => {
      this.UserDetailIdUpdate = null;
      this.GetAllUserDetails();
    });
  }
  DeleteUserDetail(Id: string) {
    this.UserDetailservice.DeleteUserDetail(Id).subscribe(() => {
      this.UserDetailIdUpdate = null;
      this.GetAllUserDetails();
    });
  }
} 
