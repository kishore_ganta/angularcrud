import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserDetailDTO } from '../app/UserDetailDTO';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserDetailService {
  ApiUrl = 'http://localhost:44333/';
  constructor(private httpclient: HttpClient) { }

  GetUserDetails(): Observable<UserDetailDTO[]> {
    return this.httpclient.get<UserDetailDTO[]>(this.ApiUrl + 'Api/UserDetail/GetUserDetails');
  }

  GetUserDetailById(Id: string): Observable<UserDetailDTO> {
    return this.httpclient.get<UserDetailDTO>(this.ApiUrl + 'Api/UserDetail/GetUserDetailById/' + Id);
  }
  InsertUserDetail(UserDetail: UserDetailDTO) {
    return this.httpclient.post<UserDetailDTO>(this.ApiUrl + 'Api/UserDetail/InsertUserDetail', UserDetail);
  }

  UpdateUserDetail(UserDetail: UserDetailDTO): Observable<UserDetailDTO> {
    return this.httpclient.put<UserDetailDTO>(this.ApiUrl + 'Api/UserDetail/UpdateUserDetail/', UserDetail);
  }

  DeleteUserDetail(Id: string) {
    return this.httpclient.delete(this.ApiUrl + 'Api/UserDetail/DeleteUserDetail/' + Id);
  }

  
} 
