﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularCrud.Models
{
    public class AngularCrudDbContext: DbContext
    {
        public AngularCrudDbContext(DbContextOptions options) : base(options)
        {
            
        }

        public DbSet<UserDetail> UserDetail { get; set; }
    }
}
