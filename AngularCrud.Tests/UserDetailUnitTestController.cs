﻿using AngularCrud.Controllers;
using AngularCrud.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace AngularCrud.Tests
{
    public class UserDetailUnitTestController
    {
        public static DbContextOptions<AngularCrudDbContext> dbContextOptions { get; set; }
        public AngularCrudDbContext context { get; set; }

        public UserDetailUnitTestController()
        {
            dbContextOptions = new DbContextOptionsBuilder<AngularCrudDbContext>()
                .UseInMemoryDatabase(databaseName: "AngularCrudDatabase")
                .Options;

            context = new AngularCrudDbContext(dbContextOptions);

            if (context.UserDetail!=null && context.UserDetail.Count()==0)
            {
                context.UserDetail.AddRange
                    (
                   new UserDetail() { ID = 1, UserName = "user1", Password = "password1" },
                   new UserDetail() { ID = 2, UserName = "user2", Password = "password2" },
                   new UserDetail() { ID = 3, UserName = "user3", Password = "password3" },
                   new UserDetail() { ID = 4, UserName = "user4", Password = "password4" },
                   new UserDetail() { ID = 5, UserName = "user5", Password = "password5" }
               );

                context.SaveChanges();
            }
           
        }

        [Fact]
        public void GetAllUserDetails()
        {
            List<UserDetail> movies = context.UserDetail.ToList();
            Assert.Equal(5, movies.Count);   
        }
              

        #region Get By Id        

        [Fact]
        public async void Task_GetUserDetailsById_Return_NotFoundResult()
        {
            //Arrange  
            var controller = new UserDetailsController(context);
            var id = 6;

            //Act  
            var data = await controller.GetUserDetail(id);

            //Assert  
            Assert.IsType<NotFoundResult>(data.Result);
        }
    

        [Fact]
        public async void Task_GetUserDetailsById_MatchResult()
        {
            //Arrange  
            var controller = new UserDetailsController(context);
            int id = 1;

            //Act  
            var result = await controller.GetUserDetail(id);

            var data = result.Value;

            Assert.Equal("user1", data.UserName);
            Assert.Equal("password1", data.Password);
        }

        #endregion
    }
}
